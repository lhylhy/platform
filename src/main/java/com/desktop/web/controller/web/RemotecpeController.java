/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.desktop.web.core.aop.RightTarget;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.remotecpe.NatInfo;
import com.desktop.web.service.remotecpe.RemotecpeService;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Controller
@RequestMapping("/webapi")
public class RemotecpeController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RemotecpeService remotecpeService;

    @RequestMapping(value = "/remotecpe/shutdown", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_remotecpe")
    public Object shutdown(HttpServletRequest request) {

        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            User user = this.getCurUser();
            remotecpeService.shutdownDevice(user, Long.valueOf(params.get("deviceId")));
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/remotecpe/start", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_remotecpe")
    public Object startRemotecpe(HttpServletRequest request) {

        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            User user = this.getCurUser();
            NatInfo natInfo = remotecpeService.startRemoteControl(user, params);
            Map<String, Object> ret = new HashMap<String, Object>();
            ret.put("uuid", natInfo.getUuid());
            return Result.Success(ret);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/remotecpe/ping", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_remotecpe")
    public Object ping(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            remotecpeService.ping(params.get("uuid").toString());
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }
}
