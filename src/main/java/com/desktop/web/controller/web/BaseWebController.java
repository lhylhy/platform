/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import org.springframework.beans.factory.annotation.Autowired;

import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.service.user.UserService;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
abstract class BaseWebController {

    @Autowired
    private UserService userService;

    /**
     * 获取用户信息
     * 
     * @return
     */
    protected User getCurUser() {

        Long uid = RequestUtil.getUid();
        if (uid == null) {
            throw new BusinessException("无授权信息");
        }

        User user = userService.getUserById(uid);
        if (user == null) {
            throw new BusinessException("无授权信息");
        }

        return user;
    }
}
