/**
 * 
 */
package com.desktop.web.uda.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.desktop.web.core.db.BaseEntity;

/**
 * @author baibai
 *
 */
@TableName("t_role_right")
public class RoleRight extends BaseEntity {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    private Long roleId;
    private Long rightId;
    private String rightTarget;

    /**
     * @return the roleId
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the rightId
     */
    public Long getRightId() {
        return rightId;
    }

    /**
     * @param rightId the rightId to set
     */
    public void setRightId(Long rightId) {
        this.rightId = rightId;
    }

    /**
     * @return the rightTarget
     */
    public String getRightTarget() {
        return rightTarget;
    }

    /**
     * @param rightTarget the rightTarget to set
     */
    public void setRightTarget(String rightTarget) {
        this.rightTarget = rightTarget;
    }

}
