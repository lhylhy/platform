/*
 * $Revision$
 * $Date$
 *
 * Copyright (C) 2015-2018 loon.com. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   loon  2016年11月30日 下午3:24:16  created
 */
package com.desktop.web.uda.mapper;

import com.desktop.web.core.db.BaseMapper;
import com.desktop.web.uda.entity.Node;

/**
 * 
 *
 * @author baibai
 */
public interface NodeMapper extends BaseMapper<Node> {

}
